
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Methods {
	
	public static Result clickProject(WebDriver driver, String projectName) {
		Result result = new Result(0, "");
		
		try {
			driver.findElement(By.xpath(".//html/body//table//a[contains(., '" + projectName + "')]")).click();
		} catch (Exception e) {
			result.setmessage("Failed to click on the project.");
			return result;
		}
		
		result.setpF(1);
		result.setmessage("clickProject() was successful.");
		return result;
	}
	
	public static Result validateProjectName(WebDriver driver, String projectName) {
		Result result = new Result(0, "");
		
		try {
			if (!driver.findElement(By.xpath(".//*[@id='project-info ']//div[contains(., '" + projectName + "')]")).isDisplayed()) {
				result.setmessage("Failed to locate the project name.");
				return result;
			}
		} catch (Exception e) {
			result.setmessage("Failed to locate the project name.");
			return result;
		}
		
		result.setpF(1);
		result.setmessage("validateProjectName() was successful.");
		return result;
	}
}


public class Result {

	// 0 = False, 1 = True
	int pF;
	String message;

	public int getpF() {
		return pF;
	}
	
	public void setpF(int pF) {
		this.pF = pF;
	}
	
	public String getmessage() {
		return message;
	}
	
	public void setmessage(String message) {
		this.message = message;
	}
	
	public Result(int pF, String message) {
		
		this.pF = pF;
		this.message = message;
	}
}

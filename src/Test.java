
public class Test {

	String name;
	Result result;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}
	
	public Test (String name, Result result) {
		
		this.name = name;
		this.result = result;
	}
}

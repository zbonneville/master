import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Test_Cases {

	public static Test testCase1() {
		
		try {
			System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\Java\\geckodriver-v0.16.1-win64\\geckodriver.exe");
			WebDriver driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			driver.get("http://pqa-0210pc.pqatesting.com:4000/");
			
			Test testCase1 = new Test("testCase1", null);
			Result result;
			
			result = Methods.clickProject(driver, "Boom boom");
			if (result.pF == 0) {
				testCase1.result = result;
				driver.close();
				return testCase1;
			}
			
			result = Methods.validateProjectName(driver, "Boom boom");
			if (result.pF == 0) {
				testCase1.result = result;
				driver.close();
				return testCase1;
			}
			
			driver.close();
			testCase1.setResult(new Result(1, "testCase1 was successful."));
			return testCase1;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
